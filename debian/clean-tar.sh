#!/bin/bash

# call with $0 --version whatever tarfile


mk-origtargz --package unrtf -S -clean  \
     --exclude-file=Makefile         \
     --exclude-file=autom4te.cache/* \
     --exclude-file=config/*         \
     --exclude-file=config.h         \
     --exclude-file=config.log       \
     --exclude-file=config.status    \
     --exclude-file=doc/Makefile     \
     --exclude-file=outputs/Makefile \
     --exclude-file=patches/Makefile \
     --exclude-file=src/.deps/*.Po   \
     --exclude-file=src/Makefile     \
     --exclude-file=stamp-h1         \
     --exclude-file=tests/Makefile   \
     --exclude-file=tests/.~lock.*#  \
     $*
